from flask import Flask, redirect, url_for, request, render_template, make_response, flash, session
import random
import string
from datetime import datetime


class Board:
    """Class representing the board that should be locked."""
    booked = False
    owner = ""
    token = ""
    booked_at = ""
    expected_duration = ""

    magic_word = "IMSURE"

    def get_magic_word(self):
        return self.magic_word

    def info(self):
        if self.booked:
            return "Board booked by {} at {}. Expected duration: {} minutes.".format(self.owner, self.booked_at, self.expected_duration)
        else:
            return "Board is free."

    def is_booked(self):
        return self.booked

    def book(self, username, expected_duration):
        if not self.booked:
            if username != "":
                self.owner = username
            else:
                raise RuntimeError("ERROR: No username given!")
            self.booked = True
            self.expected_duration = expected_duration
            self.token = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))
            self.booked_at = "{}".format(datetime.now())
            return self.token
        else:
            raise RuntimeError("ERROR: Board booked already. Current owner is {}".format(self.owner))

    def release(self, token):
        if self.booked and token == self.token:
            self.booked = False
            self.owner = ""
            self.token = ""
            self.booked_at = ""
        elif token != self.token:
            raise RuntimeError("ERROR: Token not valid.")
        elif not self.booked:
            return
        else:
            raise RuntimeError("ERROR: Unexpected condition encountered.")

    def force_release(self):
        self.booked = False
        self.owner = ""
        self.token = ""
        self.booked_at = ""



app = Flask(__name__)
app.secret_key = "FDSdgfgsdgfgds"
board = Board()

@app.route('/api')
@app.route('/')
def index():
    if request.path == '/api':
        return board.info()
    else:
        return render_template('book.html', status=board.info(), locked=board.is_booked(), expected_magic=board.get_magic_word()) 

@app.route('/book/api', methods=['POST'])
@app.route('/book', methods=['POST'])
def book():
    username = request.form['username']  # Using this instead of .get will trigger a 400 error if no username is provided.
    duration = request.form['duration']
    try:
        token = board.book(username, duration)
        if request.path == '/book/api':
            return token
        else:
            flash("Token: {}".format(token), 'INFO')
            return redirect(url_for("index"))
    except Exception as e:
        app.logger.error("{}".format(e))
        return redirect(url_for("index"))

@app.route('/release/api', methods=['POST'])
@app.route('/release', methods=['POST'])
def release():
    token = request.form['token']
    try:
        board.release(token)
        if request.path == '/release/api':
            return "0"
        else:
            flash("Success.", 'INFO')
            return redirect(url_for("index"))
    except Exception as e:
        app.logger.error("{}".format(e))
        msg = "Couldn't release the board."
        if request.path == '/release/api':
            return "0"
        else:
            flash(msg, 'ERROR')
            return redirect(url_for("index"))

@app.route('/force_release/api', methods=['POST'])
@app.route('/force_release', methods=['POST'])
def force_release():
    magic = request.form['magic']
    if magic == board.get_magic_word():
        board.force_release()
        if request.path == '/force_release/api':
            return "0"
        else:
            flash("Success.", 'INFO')
            return redirect(url_for("index"))
    else:
        msg = "If you really want to force the release use the phrase {}.".format(board.get_magic_word())
        if request.path == '/force_release/api':
            return msg 
        else:
            flash(msg, 'ERROR')
            return redirect(url_for("index"))

if __name__ == '__main__':
    app.run(debug=True)
